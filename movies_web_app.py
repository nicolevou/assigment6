from __future__ import print_function
import os
import time
import sys 

from flask import Markup
from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application
message =''


def get_db_creds():
    db = 'testdb'
    username = 'root'
    password = 'testpassword123@!#'
    hostname = '35.225.250.53'
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies9(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year text, title text, actor text, director text, release_date text, rating DOUBLE(16,4), ratingtext text, lowertitle text, loweractor text, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    cur.execute("INSERT INTO message (greeting) values ('Hello, World!')")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    cur.execute("SELECT * FROM movies9")
    for row in cur:
        print(row)
    entries = ''
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    global message
    print("Received request.", file=sys.stderr)
    print("who + " + request.form['delete_title'])
    title = request.form['delete_title']
    lowertitle = title.lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    cnx2 = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cnx2 = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        message = ("Movie " + title + " could not be deleted - " + Exception)
        return hello()

    cur = cnx.cursor()

    cur.execute("SELECT COUNT(*) FROM movies9 WHERE lowertitle=('" + lowertitle + "')")
    if(cur.fetchone()[0] == 0):
        message = "Movie with " + title + " does not exist"
        return hello()

    cur = cnx2.cursor()

    cur.execute("DELETE FROM movies9 WHERE lowertitle=('" + lowertitle + "')")
    cnx2.commit()

    message = "Movie " + title + " successfully deleted"

    return hello()

@app.route('/search_movie', methods=['POST'])
def search_movie():
    global message
    actor = request.form['search_actor']
    loweractor = actor.lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT title, year FROM movies9 WHERE loweractor=('" + loweractor + "')")
    results = ''
    for row in cur.fetchall():
        title, year = row
        results += title + ", " + year + ", " + actor + "<br>"
    message = Markup(results)
    return hello()

@app.route('/update_movie', methods=['POST'])
def update_movie():
    global message
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = float(request.form['rating'])

    lowertitle = title.lower()
    loweractor = actor.lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    cnx2 = ''
    cnx3 = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cnx2 = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cnx3 = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        message = "Movie " + title + " could not be updated."
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT COUNT(*) FROM movies9 WHERE lowertitle=('" + lowertitle + "')")
    if(cur.fetchone()[0] == 0):
        message = "Movie with " + title + " does not exist"
        return hello()

    cur = cnx2.cursor()

    cur.execute("DELETE FROM movies9 WHERE lowertitle=('" + lowertitle + "')")
    cnx2.commit()

    cur = cnx3.cursor()

    cur.execute("""INSERT INTO movies9 (year, title, director, actor, release_date, rating, ratingtext, lowertitle, loweractor) values (%s, %s, %s, %s, %s, %s, %s, %s, %s)""", (year, title, director, actor, release_date, rating, rating, lowertitle, loweractor))
    cnx3.commit()
    message = "Movie " + title + " successfully updated."
    return hello()

@app.route('/insert_movie', methods=['POST'])
def insert_movie():
    global message
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = float(request.form['rating'])

    lowertitle = title.lower()
    loweractor = actor.lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        message = "Movie " + title + " could not be inserted."
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("""INSERT INTO movies9 (year, title, director, actor, release_date, rating, ratingtext, lowertitle, loweractor) values (%s, %s, %s, %s, %s, %s, %s, %s, %s)""", (year, title, director, actor, release_date, rating, rating, lowertitle, loweractor))
    cnx.commit()
    message = "Movie " + title + " successfully inserted."
    return hello()

@app.route('/highest_rating', methods=['POST'])
def highest_rating():
    global message

    db, username, password, hostname = get_db_creds()

    cnx = ''
    cnx2 = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cnx2 = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT MAX(rating) FROM movies9")
    row = cur.fetchone()
    highest = str(float(row[0]))

    cur = cnx2.cursor()
    cur.execute("SELECT title, year, actor, director, rating FROM movies9 WHERE ratingtext=('" + highest + "')")
    results = ''

    for row in cur.fetchall():
        title, year, actor, director, rating = row
        results += title + ", " + year + ", " + actor + ", " + director + ", " + str(rating) + "<br>"
    message = Markup(results)
    return hello()

@app.route('/lowest_rating', methods=['POST'])
def lowest_rating():
    global message

    db, username, password, hostname = get_db_creds()

    cnx = ''
    cnx2 = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cnx2 = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT MIN(rating) FROM movies9")
    row = cur.fetchone()
    highest = str(float(row[0]))

    cur = cnx2.cursor()
    cur.execute("SELECT title, year, actor, director, rating FROM movies9 WHERE ratingtext=('" + highest + "')")
    results = ''

    for row in cur.fetchall():
        title, year, actor, director, rating = row
        results += title + ", " + year + ", " + actor + ", " + director + ", " + str(rating) + "<br>"
    message = Markup(results) + highest
    return hello()

@app.route("/")
def hello():
    global message
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries, message=message)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
